#!/usr/bin/python3

def factorial(numero):
    fact = 1
    while numero > 1:
        fact = fact * numero
        numero = numero - 1
    return fact


if __name__ == "__main__":
    # aqui va lo que se ejecuta primero
    for i in range(10):
        print(factorial(i))
