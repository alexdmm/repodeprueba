#!/usr/bin/python3
from sys import argv
from factorial import factorial


def suma(n1, n2):
    return n1 + n2


if __name__ == "__main__":
    if argv[1] == "factorial":
        # ejecutar el factorial del segundo argumento, pasar a entero
        fact = factorial(int(argv[2]))
        print(fact)
    elif argv[1] == "suma":
        try:
            s = suma(int(argv[2]), int(argv[3]))
            print(s)
        except ValueError:
            print("Escribe bien los artumentos por favor.")
